<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  version="2.0">
  
  <!-- This filter removes attributes on the root of a delta, so that when
       a move result is 'stitched back' into a output filter chain the
       top-level status attributes are removed and not stictched into
       the result -->
  
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@deltaxml:version | @deltaxml:content-type"/>

</xsl:stylesheet>