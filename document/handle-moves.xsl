<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:samplejava="java:com.deltaxml.samples.ProcessMoves"
  xmlns="http://docbook.org/ns/docbook" 
  xpath-default-namespace="http://docbook.org/ns/docbook"
  version="2.0">
  
  <xsl:include href="../functions/nearest-delta.xsl"/>
  
  <!-- This filter detects and processes author 'moves'.  A move is defined
    as exactly one added and one deleted author with the same xml:id attribute.
    Keys are used to provide efficiency and count adds/deletes.
    We support moves anywhere within the document.  -->
  
  <xsl:key name="deletedAuthors" match="author[deltaxml:nearest-delta-is(.,'A')]" use="@xml:id"/>
  <xsl:key name="addedAuthors" match="author[deltaxml:nearest-delta-is(.,'B')]" use="@xml:id"/>
  
  <xsl:template match="@* | node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="author[deltaxml:nearest-delta-is(.,'B')]
                              [count(key('deletedAuthors', @xml:id)) eq 1 and count(key('addedAuthors', @xml:id)) eq 1]">
    <xsl:copy>
      <!-- if you don't care about changes to moved authors you could just apply-templates now,
            otherwise need to re-compare the added/deleted author using the compare extension function -->
      <xsl:variable name="a" select="key('deletedAuthors', @xml:id)" as="node()"/>
      <xsl:variable name="b" select="." as="node()"/>
      <xsl:variable name="delta" as="node()"
          select="samplejava:compare($a, $b)"/>
      <xsl:apply-templates select="$delta/*/@*"/>
      <xsl:comment>This author element was detected as a move</xsl:comment>
      <xsl:apply-templates select="$delta/*/node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="author[deltaxml:nearest-delta-is(.,'A')]
                              [count(key('deletedAuthors', @xml:id)) eq 1 and count(key('addedAuthors', @xml:id)) eq 1]">
    <xsl:comment>An author element with xml:id <xsl:value-of select="@xml:id"/> was at this location but has been moved</xsl:comment>
  </xsl:template>
</xsl:stylesheet>