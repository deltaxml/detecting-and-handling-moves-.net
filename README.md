# Detecting and Handling Moves

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/detecting-and-handling-moves`.*

---

An example pipeline showing how to post-process a delta file to detect and mark data that has been moved.

This document describes how to run the sample. For concept details see: [Detecting and Handling Moves](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/detecting-and-handling-moves)

## Running the sample

The sample can be run via a run.bat batch file, so long as this is issued from the sample directory. Alternatively, as this batch file contains a single command, the command can be executed directly:

	..\..\bin\deltaxml.exe compare moves r1.xml r2.xml result.xml
		
# **Note - .NET support has been deprecated as of version 10.0.0 **